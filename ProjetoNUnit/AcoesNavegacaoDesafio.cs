using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjetoNUnit
{
    [TestFixture]
    public class AcoesNavegacaoDesafio
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void NavegaParaGoogle()
        {
            driver.Navigate().GoToUrl("https://www.google.com/");
        }

        [Test]
        public void AssertEstaNoGoogle()
        {
            driver.Navigate().GoToUrl("https://www.google.com/");

            Assert.IsTrue(driver.Title.Equals("Google"));   
        }

        [Test]
        public void AssertLinkGoogle()
        {
            driver.Navigate().GoToUrl("https://www.google.com/");

            Assert.IsTrue(driver.Url.Equals("https://www.google.com/"));
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}