using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace ProjetoNUnit
{
    [TestFixture]
    public class LocalizandoElementosComEstrategiasDesafio
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void AssertPesquisaGoogle()
        {
            string busca = "carro";

            driver.Navigate().GoToUrl("https://www.google.com/");

            IWebElement inputPesquisa = driver.FindElement(By.Name("q"));

            inputPesquisa.SendKeys(busca + Keys.Enter);

            Assert.IsTrue(driver.PageSource.Contains("Carros"));

        }

        [Test]
        public void AssertPreencheFormulario() 
        {
            Random rdn = new Random();
            string username = $"Mila - {rdn.Next(100)}";

            driver.Navigate().GoToUrl("http://thedemosite.co.uk/savedata.php");

            IWebElement inputUsername = driver.FindElement(By.Name("username"));
            IWebElement inputPassword = driver.FindElement(By.Name("password"));
            IWebElement btnSave = driver.FindElement(By.Name("FormsButton2"));

            inputUsername.SendKeys(username);
            inputPassword.SendKeys("Senha");
            btnSave.Click();

            Assert.IsTrue(driver.PageSource.Contains(username));//Tirar dúvida se seria o melhor assert usando PageSource   
        }

        [Test]
        public void AssertCampoObrigatorio()
        {
            Random rdn = new Random();
            string username = $"Mila - {rdn.Next(100)}";

            driver.Navigate().GoToUrl("http://thedemosite.co.uk/savedata.php");

            IWebElement inputUsername = driver.FindElement(By.Name("username"));
            IWebElement btnSave = driver.FindElement(By.Name("FormsButton2"));

            inputUsername.SendKeys(username);
            btnSave.Click();

            IAlert alert = driver.SwitchTo().Alert();
            string alertText = alert.Text;

            Assert.AreEqual(alertText, "Password too short.  The password must be at least 4 characters in length."); 
        }

        /* Deve realizar busca de voos de Dublin para Frankfurt 
           com data de partida na data atual e data de retorno daqui a dois dias
         */
        [Test]
        public void MultiplasInteracoesDeNavegacao()
        {
            string from = "Dublin";
            string fromAbreviacao = "LHE";
            string to = "Frankfurt";
            string toAbreviacao = "DXB";
            string diaPartida = $"{DateTime.Today.Day}";
            string diaRetorno = $"{DateTime.Today.AddDays(2).Day}";

            IJavaScriptExecutor js = (IJavaScriptExecutor) driver;

            //Acessa site de viagens
            driver.Navigate().GoToUrl("https://www.phptravels.net/home");
            driver.Manage().Window.Maximize();

            //Seleciona busca de voos
            IWebElement btnFlight = driver.FindElement(By.XPath("//nav/ul/li[2]/a"));

            btnFlight.Click();
            
            //Seleciona pesquisa de ida e volta
            IWebElement radioRoundTrip = driver.FindElement(By.XPath("//div[2]/label"));
            radioRoundTrip.Click();

            //Seleciona classe de voo (Business)
            IWebElement selectClass = driver.FindElement(By.CssSelector(".form-icon-left.flightclass"));//Tirar dúvida sobre melhor forma de fazer
            selectClass.Click();
            IWebElement optionClass = driver.FindElement(By.XPath("//ul[@class = 'chosen-results']/li[2]"));
            optionClass.Click();
            
            //Determina partida e destino
            IWebElement inputFrom = driver.FindElement(By.Id("s2id_autogen9"));
            IWebElement inputTo = driver.FindElement(By.Id("s2id_autogen10"));
            inputFrom.SendKeys(from + Keys.Tab);
            inputTo.SendKeys(to + Keys.Tab);

            //Seleciona data de ida
            IWebElement datePickerDepart = driver.FindElement(By.Name("departure_date"));
            datePickerDepart.Click();

            IWebElement dateDepart = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.XPath($"//div[@id='datepickers-container']/div[9]/div/div/div[2]/div[text() = '{diaPartida}']")));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", dateDepart);
            
            dateDepart.Click();

            //Seleciona data de volta
            IWebElement datePickerReturn = driver.FindElement(By.Name("reture_date"));
            datePickerReturn.Click();

            IWebElement dateReturn = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.XPath($"//div[@id='datepickers-container']/div[10]/div/div/div[2]/div[text() = '{diaRetorno}']")));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", dateReturn);
            
            dateReturn.Click();

            //Realiza a busca
            IWebElement btnSearch = driver.FindElement(By.XPath("//form/div/div/div[3]/div[4]/button"));
            btnSearch.Click();
                        
            //Verifica busca
            IWebElement searchTitle = driver.FindElement(By.ClassName("heading-title"));
            string textoTitulo = searchTitle.Text;

            Assert.AreEqual(textoTitulo, $"{fromAbreviacao} to {toAbreviacao}");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}