using NUnit.Framework;

namespace ProjetoNUnit
{
    [TestFixture]
    public class NunitDesafio
    {

        [Test]
        public void AssertIsTrue()
        {
            string nome = "Mila";

            Assert.IsTrue(nome.EndsWith("a"));
        }

        [Test]
        public void AssertIsFalse()
        {
            string nome = "Mila";

            Assert.IsFalse(nome.Equals("mila"));
        }

        [Test]
        public void AssertAreEqual()
        {
            string nome = "Mila";

            Assert.AreEqual(nome, "Mila");
        }
    }
}