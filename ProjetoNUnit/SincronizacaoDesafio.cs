using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace ProjetoNUnit
{
    [TestFixture]
    public class SincronizacaoDesafio
    {
        IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void SelecionaOpcaoDropdown()
        {
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/dropdown");

            SelectElement dropDown = new SelectElement(driver.FindElement(By.Id("dropdown")));

            dropDown.SelectByText("Option 1");

        }

        [Test]
        public void AcessarMeusCasosDeTestes()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor) driver;

            driver.Navigate().GoToUrl("http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth");
            driver.Manage().Window.Maximize();

            IWebElement inputLogin = driver.FindElement(By.Id("login"));
            IWebElement inputSenha = driver.FindElement(By.Id("password"));
            IWebElement btnEntrar = driver.FindElement(By.XPath("//form/div[3]/button"));

            inputLogin.SendKeys("mila.nozella@base2.com.br");
            inputSenha.SendKeys("base2@123");
            btnEntrar.Click();

            IWebElement btnGerenciar = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.CssSelector(".btn.btn-crowdtest.mr-1")));

            js.ExecuteScript("arguments[0].scrollIntoView(true);", btnGerenciar);

            btnGerenciar.Click();

            IWebElement selectProjeto = driver.FindElement(By.Id("id"));
            selectProjeto.Click();

            IWebElement optionProjeto = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.XPath("//span[text() = 'Projeto 01']")));
            optionProjeto.Click();

            IWebElement detalharCasos = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.XPath("//app-manager-dashboard-cards/div/div/div[4]/div/div[2]/div[3]/span[contains(text(), 'Detalhes')]")));

            detalharCasos.Click();

            IWebElement tituloCasoDeTeste = new WebDriverWait(driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions
                .ElementIsVisible(By.XPath("//mat-cell[2]")));

            string textoTitulo = tituloCasoDeTeste.Text;

            Assert.AreEqual(textoTitulo, "CT-01");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}